const readline = require('readline')
const rl = readline.createInterface({
    input: process.stdin,
    output: process.stdout
});

let scores = [];

function getInput() {
    console.log('Inputkan dan ketik "q" jika sudah selesai\n')
    rl.on('line', (line) => {
        if (line === 'q') {
            rl.close();
        } else {
            scores.push(Number(line));
        }
    }).on('close', () => {
        let dataSorted = bubbleSort(scores);
        console.log("\nSelesai memasukkan nilai, cari outputnya:\n");
        console.log(`▪ Nilai tertinggi : ${Math.max(...scores)}`);
        console.log(`▪ Nilai terendah : ${Math.min(...scores)}`);
        console.log(`▪ Urutan nilai dari terendah ke tertinggi : ${dataSorted}`);
        console.log(`▪ Rata-rata : ${dataSorted.reduce((a, b) => a + b) / dataSorted.length}`);
        console.log(`▪ Jumlah siswa yang lulus : ${validatePass(dataSorted, 'pass')}`)
        console.log(`▪ Jumlah siswa yang tidak lulus : ${validatePass(dataSorted, 'noPass')}`)
    });
}

function bubbleSort(arr) {
    for (let i = 0; i < arr.length; i++) {
        for (let j = 0; j < arr.length - 1; j++) {
            if (arr[j] >= arr[j + 1]) {
                temp = arr[j]
                arr[j] = arr[j + 1]
                arr[j + 1] = temp
            }
        }
    }
    return arr
}

function validatePass(arr, keyword) {
    let pass = [];
    let noPass = []
    for (let i = 0; i < arr.length; i++) {
        if (arr[i] >= 75) {
            pass.push(arr[i])
        } else {
            noPass.push(arr[i])
        }
    }
    return keyword === 'pass' ? pass.length : noPass.length;
}

getInput();